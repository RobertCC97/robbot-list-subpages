=== Robbot List Subpages ===
Contributors: RobertCC

Tags: page, pages, post, formatting, list, shortcode
Requires at least: 2.5.1
Tested up to: 4.8
Stable tag: 1.3

Adds a tag that you can use in your pages to display a list of it's subpages.

== Description ==

Are you struggling with long page loads due to trying to list child pages with a large database?

We have created a plugin that will list WP child pages with a user defined depth of up to 3. Our plugin uses direct database calls to make for the fastest page loading times available, the plugin will soon be available through the Wordpress Plugin database but is available for early use here!

Benefits

    Faster page load times
    Direct Database calls
    Choice of page or post to avoid type conflicts


Usage Instructions

    Download plugin zip file
    Go to your add new plugins page and choose the upload plugin option
    Select your zip folder and install
    On the parent post or page that you would like to list pages on go to edit page
    Use the shortcode [bot-child depth='1' type='page'] to get a list of pages

Shortcode Options

    depth : 1 - 3
    type : post or page (beta: may support custom post types)

For support please visit our forum and start a new topic with your problem here


== Frequently Asked Questions ==

